package com.mycompany.gitanalysis.app;

import static javafx.application.Application.launch;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App {

    static EntityManagerFactory factory = Persistence.createEntityManagerFactory("GitAnalysis");
    static EntityManager entityManager;
        
    public static EntityManager getEM() {
        return entityManager;
    }

    public static void main(String[] args) {        
        startConnection();         
        launch(JfxApp.class, args); 
        closeConnection();        
    }

    /** Close connection with database */
    public static void closeConnection() {
        entityManager.getTransaction().commit();
        entityManager.close();
        factory.close();
    }

    /** Start connection with database */
    public static void startConnection() {
        entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
    }
}
