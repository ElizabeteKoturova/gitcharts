package com.mycompany.gitanalysis.app;

import static com.mycompany.gitanalysis.dao.Count.getAllBranches;
import static com.mycompany.gitanalysis.dao.Database.insertRepo;
import static com.mycompany.gitanalysis.commits.GitCommits.logCommits;
import static com.mycompany.gitanalysis.dao.Count.getLastCommits;
import com.mycompany.gitanalysis.database.Commit;
import com.mycompany.gitanalysis.database.Repo;
import static com.mycompany.gitanalysis.reports.AddDelChart.getAddDelChart;
import static com.mycompany.gitanalysis.reports.ChartArea.getAreaChart;
import static com.mycompany.gitanalysis.reports.CommitSearch.createHash;
import static com.mycompany.gitanalysis.reports.CommitSearch.search;
import static com.mycompany.gitanalysis.reports.WeekDayHourChart.getWeekdayChart;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.sql.Date;
import java.util.List;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevWalk;
import static com.mycompany.gitanalysis.reports.ChartPie.getPieChart;

public class JfxApp extends Application {

    public static Repository setRepo() {
        return repo;
    }

    public static Repo setRepoDB() {
        return repoDB;
    }

    public static int setRepo_id() {
        return repoDB.getID();
    }

    public static EntityManager getEM() {
        return entityManager;
    }

    public static RevWalk setRevWalk() {
        return walk;
    }

    public static Git setGit() {
        return git;
    }

    static EntityManager entityManager = getEM();
    static Repo repoDB;
    static Repository repo;
    static Git git;
    static RevWalk walk;
    static List<Ref> branches;

    BorderPane border;
    static LocalDate value;
    static LocalDate value1;
    static String year;
    String path;
    BufferedImage bufferedImage = new BufferedImage(550, 400, BufferedImage.TYPE_INT_ARGB);
    File file;
    VBox vbox = null;
    Label label2;
    String[] hashes;
    String arial = "Arial";

    /**
     * Application look
     */
    @Override
    public void start(Stage stage) throws FileNotFoundException, IOException {
        border = new BorderPane();
        border.setTop(addPath());
        border.setLeft(addVBox());
        border.setRight(addTextField());
        Scene scene = new Scene(border);
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.setTitle("GitAnalysis");
        stage.show();

        Alert alert = new Alert(AlertType.CONFIRMATION);     //Dialog log to choose repository
        alert.setTitle("GitAnalysis");
        alert.setHeaderText("Choose your repository");
        alert.setResizable(true);
        GridPane grid = new GridPane();
        grid.setMinSize(500, 200);
        grid.setPadding(new Insets(10, 10, 10, 10)); //Setting the vertical and horizontal gaps between the columns 
        grid.setVgap(10);
        grid.setHgap(10);                            //Setting the Grid alignment  
        Label label1 = new Label("Selected repository path: ");

        final Button browseButton = new Button("Browse Directory");
        browseButton.setOnAction((final ActionEvent e) -> {
            final DirectoryChooser directoryChooser = new DirectoryChooser();
            final File selectedDirectory = directoryChooser.showDialog(null);
            if (selectedDirectory != null) {
                path = getPath(selectedDirectory.toString());   //Variable to save choosed path
                label2 = new Label(path);
                grid.add(label2, 1, 2);                         //Label shows selected path
                try {
                    repo = new FileRepository(path);            //Create git repository
                    repoDB = insertRepo(path);                  //Save repository in db
                    git = new Git(repo);                        //Git variables, to find all branches and commits in repo
                    walk = new RevWalk(repo);
                    branches = git.branchList().call();
                } catch (IOException | GitAPIException ex) {
                    Logger.getLogger(JfxApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        grid.add(browseButton, 0, 0);
        grid.add(label1, 0, 2);

        alert.getDialogPane().setContent(grid);
        alert.showAndWait();
        border.setCenter(repositoryInfo());
    }

    /**
     * Menu view
     */
    private VBox addVBox() throws FileNotFoundException {
        Text title = new Text("Charts");
        title.setFont(Font.font(arial, FontWeight.BOLD, 14));
        MenuItem menuItem1 = new MenuItem("Changes by Authors");
        MenuItem menuItem2 = new MenuItem("Commits by Author and Month");
        MenuItem menuItem3 = new MenuItem("Additions and Deletions Chart");
        MenuItem menuItem4 = new MenuItem("Changes by Week Day and Hour");
        MenuItem menuItem5 = new MenuItem("Commit search");

        FileInputStream input = new FileInputStream("src\\main\\resources\\images\\52152.png");
        Image image = new Image(input);
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(90);
        imageView.setFitWidth(90);

        MenuButton menuButton = new MenuButton("Reports", imageView, menuItem1, menuItem2, menuItem3, menuItem4, menuItem5);
        menuButton.autosize();
        menuItem1.setOnAction((ActionEvent event) -> {
            vbox = new VBox(getPieChart());
            border.setCenter(vbox);
        });

        menuItem2.setOnAction((ActionEvent event) -> {
            vbox = new VBox(getAreaChart());
            border.setCenter(vbox);
        });

        menuItem3.setOnAction((ActionEvent event) -> {
            try {
                vbox = new VBox(getAddDelChart());
                border.setCenter(vbox);
            } catch (IOException ex) {
                Logger.getLogger(JfxApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        menuItem4.setOnAction((ActionEvent event) -> {
            try {
                vbox = new VBox(getWeekdayChart());
                border.setCenter(vbox);
            } catch (IOException | GitAPIException ex) {
                Logger.getLogger(JfxApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        menuItem5.setOnAction((ActionEvent event) -> {
            try {
                vbox = new VBox(search());
                vbox.setSpacing(10);
                vbox.setPadding(new Insets(15, 12, 15, 12));
            } catch (IOException ex) {
                Logger.getLogger(JfxApp.class.getName()).log(Level.SEVERE, null, ex);
            }
            border.setCenter(vbox);
        });

        VBox menu = new VBox(menuButton);
        menu.setPadding(new Insets(15, 12, 15, 12));

        return menu;
    }

    /**
     * Text fields to create charts
     */
    private VBox addTextField() {

        Label aDChart = new Label("Additions and Deletions Chart");
        aDChart.setFont(Font.font(arial, FontWeight.BOLD, 18));
        Label label1 = new Label("Start date:");
        DatePicker startDatePicker = new DatePicker();
        Label end = new Label("End date:");
        DatePicker endDatePicker = new DatePicker();
        Label area = new Label("Commits per month");
        area.setFont(Font.font(arial, FontWeight.BOLD, 18));
        TextField textField = new TextField();
        Button button = new Button("Set Dates");

        button.setOnAction(action -> {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Set Dates");
            alert.setHeaderText(null);
            alert.setContentText("Dates set succsesfully!");
            alert.showAndWait();
            value = startDatePicker.getValue(); //Gets start and end date for Additions and Deletions chart
            value1 = endDatePicker.getValue();
        });

        Button button2 = new Button("Set Year");

        button2.setOnAction(action -> {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Set Year");
            alert.setHeaderText(null);
            alert.setContentText("Year set succsesfully!");
            alert.showAndWait();
            year = textField.getText();          //Gets year for authors and months chart
        });

        VBox fields = new VBox();
        fields.getChildren().addAll(aDChart, label1, startDatePicker, end, endDatePicker, button, area, textField, button2);
        fields.setSpacing(10);
        fields.setPadding(new Insets(15, 12, 15, 12));
        return fields;
    }

    private HBox addPath() {

        Button button2 = new Button("Update");

        button2.setOnAction(action -> {
            try {
                int update = logCommits(walk, git, branches, repo, repoDB);
                border.setCenter(repositoryInfo());
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Updated succesfully");
                alert.setHeaderText(null);
                alert.setContentText("Was added " + update + " commits!");
                alert.showAndWait();
            } catch (GitAPIException | IOException ex) {
                Logger.getLogger(JfxApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        Button btn = new Button();
        btn.setText("Save Chart");
        btn.setOnAction((ActionEvent event) -> {
            FileChooser fileChooser = new FileChooser();

            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image Files (*.jpg)", "*.jpg"); //Set extension filter
            fileChooser.getExtensionFilters().add(extFilter);

            file = fileChooser.showSaveDialog(null); //Show save file dialog

            if (file != null) {
                WritableImage snapshot = vbox.snapshot(new SnapshotParameters(), null);
                saveImage(snapshot);
            }
        });

        HBox hbox = new HBox(button2, btn);
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #BFF3FF;");

        return hbox;
    }

    static public Date getStartDateChart() {
        Date date = Date.valueOf(value);
        return new Date(date.getTime());
    }

    static public Date getEndDateChart() {
        Date date = Date.valueOf(value1);
        return new Date(date.getTime());
    }

    static public int getYear() {
        return Integer.parseInt(year);
    }

    /**
     * Transform repo path to save it in db
     */
    static public String getPath(String path) {
        path = path.replace("\\", "/");
        path += "/.git";
        return path;
    }

    private void saveImage(WritableImage snapshot) {
        BufferedImage image;
        image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, bufferedImage);
        try {
            Graphics2D gd = (Graphics2D) image.getGraphics();
            gd.translate(vbox.getWidth(), vbox.getHeight());
            ImageIO.write(image, "png", file);
        } catch (IOException ex) {
            Logger.getLogger(JfxApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    VBox branches() throws FileNotFoundException{
        VBox branchesInRepo = new VBox();
        branchesInRepo.setPadding(new Insets(15, 12, 15, 12));
        branchesInRepo.setSpacing(10);
        Label title1 = new Label("Branches:");
        title1.setFont(new Font("Constantia", 21));
        title1.setTextFill(Color.web("#0076a3"));
        List<String> branch = getAllBranches(repoDB.getID());
         for (int i = 0; i < branch.size(); i++) {
            Image branchIcon = new Image(new FileInputStream("src\\main\\resources\\images\\branch.png"));
            ImageView branchView = new ImageView(branchIcon);
            branchView.setFitHeight(40);
            branchView.setFitWidth(40);
            Label label = new Label(branch.get(i));
            label.setGraphic(branchView);
            label.setFont(new Font("Gulim", 15));
            branchesInRepo.getChildren().add(label);
        }
         return new VBox(title1, branchesInRepo);
    }
    
    /**
     * Gets repo active branches and 5 last commits
     */
    VBox repositoryInfo() throws FileNotFoundException {              
        VBox lastCommits = new VBox();
        lastCommits.setPadding(new Insets(15, 12, 15, 12));
        lastCommits.setSpacing(10);
        Label label1 = new Label(generateRepoName(path));        
        Label title2 = new Label("Last 5 Commits:");      
        title2.setFont(new Font("Constantia", 21));
        title2.setTextFill(Color.web("#0076a3"));
        FileInputStream input = new FileInputStream("src\\main\\resources\\images\\repo.png");
        Image image = new Image(input);
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(80);
        imageView.setFitWidth(80);
        label1.setGraphic(imageView);
        label1.setFont(new Font("Constantia", 30));
        label1.setTextFill(Color.web("#0076a3"));
        Line line = new Line();

        line.setStartX(100.0);
        line.setStartY(150.0);
        line.setEndX(500.0);
        line.setEndY(150.0);
        line.setStroke(Color.web("#D0D0D0"));        
        List<Commit> commit = getLastCommits(repoDB.getID());
        hashes = createHash(commit);       
        for (int i = 0; i < commit.size(); i++) {
            HBox com = new HBox();
            Image commitIcon = new Image(new FileInputStream("src\\main\\resources\\images\\commit.png"));
            ImageView commitView = new ImageView(commitIcon);
            commitView.setFitHeight(40);
            commitView.setFitWidth(40);
            Label hash = new Label(hashes[i]);
            hash.setFont(new Font("Gulim", 15));
            hash.setStyle("-fx-border-color: white;");
            Label message = new Label(commit.get(i).getMessage());
            message.setGraphic(commitView);
            message.setFont(new Font("Gulim", 15));
            com.getChildren().addAll(hash, message);
            lastCommits.getChildren().add(com);
        }
        return new VBox(label1, line, branches(), title2, lastCommits);
    }

    /**
     * Gets repo name from repo path
     */
    static String generateRepoName(String path) {
        String[] name = path.split("/");
        return name[name.length - 2];
    }
}
