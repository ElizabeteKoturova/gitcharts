package com.mycompany.gitanalysis.commits;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

/**
 * Finds difference between two commits and count changed lines
 */
public class FindDiff {

    /**
     * Gets the given commit.
     */
    static public String diffCommit(String hash, Repository repo) throws IOException {
        Git git = new Git(repo);
        RevCommit newCommit;         //Get the commit
        try (RevWalk walk = new RevWalk(repo)) {
            newCommit = walk.parseCommit(repo.resolve(hash));
        }
        return getDiffOfCommit(newCommit, git, repo);

    }

    /**
     * Gets the diff as a string.
     */
    static private String getDiffOfCommit(RevCommit newCommit, Git git, Repository repo) throws IOException {

        RevCommit oldCommit = getPrevHash(newCommit, repo); // previous commit
        if (oldCommit == null) {
            return "Start of repo";
        }
        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit, git); //Use treeIterator to diff.
        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit, git);
        OutputStream outputStream = new ByteArrayOutputStream();
        try (DiffFormatter formatter = new DiffFormatter(outputStream)) {
            formatter.setRepository(git.getRepository());
            formatter.format(oldTreeIterator, newTreeIterator);
        }
        return outputStream.toString();
    }

    /**
     * Gets the previous commit.
     */
    static public RevCommit getPrevHash(RevCommit commit, Repository repo) throws IOException {

        try (RevWalk walk = new RevWalk(repo)) {  // Starting point
            walk.markStart(commit);
            int count = 0;
            for (RevCommit rev : walk) {         // got the previous commit.
                if (count == 1) {
                    return rev;
                }
                count++;
            }
            walk.dispose();
        }
        return null;
    }

    /**
     * Gets the tree of the changes in a commit.
     */
    static private AbstractTreeIterator getCanonicalTreeParser(ObjectId commitId, Git git) throws IOException {
        try (RevWalk walk = new RevWalk(git.getRepository())) {
            RevCommit commit = walk.parseCommit(commitId);
            ObjectId treeId = commit.getTree().getId();
            try (ObjectReader reader = git.getRepository().newObjectReader()) {
                return new CanonicalTreeParser(null, reader, treeId);
            }
        }
    }

    /**
     * Gets all changed lines in commit
     */
    static public List<String> countAddDel(String hash, Repository repo) throws IOException {
        String com = diffCommit(hash, repo);
        Pattern pattern = Pattern.compile("\\n(\\+|\\-)(.)*");
        Matcher matcher = pattern.matcher(com);
        final List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            matches.add(matcher.group(0));
        }
        for (int i = 0; i < matches.size(); i++) {
            if (matches.get(i).contains("+++") || matches.get(i).contains("---")) {
                matches.remove(i--);
            }
        }
        return matches;
    }

    /**
     * Count added lines
     */
    static public int countAdditions(String hash, Repository repo) throws IOException {
        List<String> matches = countAddDel(hash, repo);
        for (int i = 0; i < matches.size(); i++) {
            if (matches.get(i).contains("-")) {
                matches.remove(i--);
            }
        }
        return matches.size();
    }

    /**
     * Count deleted lines
     */
    static public int countDeletions(String hash, Repository repo) throws IOException {
        List<String> matches = countAddDel(hash, repo);
        for (int i = 0; i < matches.size(); i++) {
            if (matches.get(i).contains("+")) {
                matches.remove(i--);
            }
        }
        return matches.size();
    }
}
