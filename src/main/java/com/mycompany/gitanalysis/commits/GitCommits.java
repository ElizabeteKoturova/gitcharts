package com.mycompany.gitanalysis.commits;

import static com.mycompany.gitanalysis.app.JfxApp.getEM;
import com.mycompany.gitanalysis.database.Author;
import com.mycompany.gitanalysis.database.Branch;
import com.mycompany.gitanalysis.database.Repo;
import static com.mycompany.gitanalysis.dao.Count.CommitsByWeekDay;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import static com.mycompany.gitanalysis.dao.Database.insertBranch;
import static com.mycompany.gitanalysis.dao.Database.insertAuthor;
import static com.mycompany.gitanalysis.dao.Database.insertCommits;
import javax.persistence.EntityManager;

public class GitCommits {

    static EntityManager entityManager = getEM();

    /**
     * Gets the repo branches and commits, and add it to db
     */
    public static int logCommits(RevWalk walk, Git git, List<Ref> branches, Repository repo, Repo repos) throws IOException, GitAPIException {
        int res = 0;
        for (Ref branch : branches) {
            String branchName = branch.getName();
            Branch branch1 = insertBranch(branchName, repos);

            Iterable<RevCommit> commits = git.log().all().call();

            for (RevCommit commit : commits) {
                boolean foundInThisBranch = false;

                RevCommit targetCommit = walk.parseCommit(repo.resolve(commit.getName()));
                for (Map.Entry<String, Ref> e : repo.getAllRefs().entrySet()) {
                    if (e.getKey().startsWith(Constants.R_HEADS) && walk.isMergedInto(targetCommit, walk.parseCommit(e.getValue().getObjectId()))) {
                        String foundInBranch = e.getValue().getName();
                        if (branchName.equals(foundInBranch)) {
                            foundInThisBranch = true;
                            break;
                        }
                    }
                }
                if (foundInThisBranch) {
                    Author author = insertAuthor(commit.getAuthorIdent().getName(), commit.getAuthorIdent().getEmailAddress());
                    java.util.Date utilStartDate = new java.util.Date(commit.getCommitTime() * 1000L);
                    if (insertCommits(commit.getName(), author, new java.sql.Date(utilStartDate.getTime()), commit.getFullMessage(), branch1)) {
                        res++;
                    }
                }
            }
        }
        return res;
    }

    /**
     * Counts commits per hour in given weekday
     */
    public static int CountCommitsPerHour(String dayname, Git git, String hour, int repo) throws GitAPIException {
        int count = 0;
        SimpleDateFormat ft = new SimpleDateFormat("HH");
        List<String> commitsHash = CommitsByWeekDay(repo, dayname);
        Iterable<RevCommit> commits = git.log().call();
        for (RevCommit commit : commits) {
            java.util.Date date = new java.util.Date(commit.getCommitTime() * 1000L);
            String commitHour = ft.format(date);
            for (int i = 0; i < commitsHash.size(); i++) {
                if (commit.getName().equals(commitsHash.get(i)) && commitHour.equals(hour)) {
                    count++;
                }
            }
        }
        return count;
    }
}
