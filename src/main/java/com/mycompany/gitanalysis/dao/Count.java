package com.mycompany.gitanalysis.dao;

import com.mycompany.gitanalysis.database.Author;
import com.mycompany.gitanalysis.database.Commit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import static com.mycompany.gitanalysis.app.App.getEM;

public class Count {

    static EntityManager entityManager = getEM();
    /**
     * This method counts commits in repository
     *
     * @param repositoryId
     * @return Repository commits number
     */
    public static long commitsCount(int repositoryId) {
        TypedQuery<Long> q = entityManager.createQuery(
                "SELECT COUNT(c.hash) FROM Commit c "
                + "JOIN Branch br ON c.branch = br.id "
                + "JOIN Repo r ON b.Repo_ID = r.id "
                + "WHERE r.id = :repositoryId", Long.class);
        q.setParameter("repositoryId", repositoryId);        
        return q.getSingleResult();           
    }

    /**
     * This method finds all authors in repository, whose commited files in
     * chosen year
     *
     * @param repoId
     * @param year
     * @return Authors email list
     */
    public static List<String> getAuthorsEmailsByRepoIDByYear(int repoId, int year) {
        TypedQuery<String> q = entityManager.createQuery("SELECT DISTINCT a.email FROM Author a "
                + "JOIN Commit c ON a.id = c.author "
                + "JOIN Branch bra ON c.branch = bra.id "
                + "JOIN Repo re ON b.Repo_ID = re.id  "
                + "WHERE re.id = :repoId "
                + "AND FUNCTION('YEAR',(c.comDate)) = :year", String.class);
        q.setParameter("repoId", repoId);
        q.setParameter("year", year);
        return q.getResultList();
    }

    public static List<String> getAuthorsEmailsByRepoId(int repoId) {
        TypedQuery<String> q = entityManager.createQuery("SELECT DISTINCT a.email FROM Author a "
                + "JOIN Commit c ON a.id = c.author "
                + "JOIN Branch ha ON c.branch = ha.id "
                + "JOIN Repo rep ON b.Repo_ID = rep.id  "
                + "WHERE rep.id = :repo_id", String.class);
        q.setParameter("repo_id", repoId);
        return q.getResultList();
    }

    public static double getCommitNumberByAuthorEmail(String email, int repositoryId) {
        Author author = getAuthorByEmail(email);
        TypedQuery<Long> q = entityManager.createQuery("SELECT COUNT(c.hash) FROM Commit c "
                + "JOIN Branch sa ON c.branch = sa.id "
                + "JOIN Repo s ON b.Repo_ID = s.id "
                + "WHERE c.author = :author "
                + "AND s.id = :repositoryId", Long.class);
        q.setParameter("author", author);
        q.setParameter("repositoryId", repositoryId);
        return q.getSingleResult();
    }

    static Author getAuthorByEmail(String email) {
        TypedQuery<Author> q = entityManager.createQuery("SELECT c FROM Author c WHERE c.email = :email", Author.class);
        q.setParameter("email", email);
        return q.getSingleResult();
    }

    public static long commitsInMonth(int year, int repo, int month, String email) {
        TypedQuery<Long> q = entityManager.createQuery("SELECT COUNT(c.hash) FROM Commit c "
                + "JOIN Author a ON c.author = a.id "
                + "JOIN Branch da ON c.branch = da.id "
                + "JOIN Repo k ON b.Repo_ID = k.id "
                + "WHERE k.id = :repo "
                + "AND FUNCTION('MONTH', c.comDate) = :month "
                + "AND FUNCTION('YEAR', c.comDate) = :year "
                + "AND a.email = :email", Long.class);
        q.setParameter("email", email);
        q.setParameter("repo", repo);
        q.setParameter("month", month);
        q.setParameter("year", year);
        return q.getSingleResult();
    }

    public static List<Commit> getCommitsByDate(int repos, java.sql.Date startDate, java.sql.Date endDate) {
        TypedQuery<Commit> q = entityManager.createQuery("SELECT c FROM Commit c "
                + "JOIN Author au ON c.author = au.id "
                + "JOIN Branch we ON c.branch = we.id "
                + "JOIN Repo p ON b.Repo_ID = p.id "
                + "WHERE p.id = :repos "
                + "AND c.comDate BETWEEN :startDate AND :endDate", Commit.class);

        q.setParameter("repos", repos);
        q.setParameter("startDate", startDate);
        q.setParameter("endDate", endDate);
        return q.getResultList();
    }

    public static List<String> CommitsByWeekDay(int repok, String dayname) {
        TypedQuery<String> q = entityManager.createQuery("SELECT c.hash FROM Commit c "
                + "JOIN Branch qw ON c.branch = qw.id "
                + "JOIN Repo d ON b.Repo_ID = d.id "
                + "WHERE d.id = :repok "
                + "AND FUNCTION('DAYNAME', c.comDate) = :dayname", String.class);

        q.setParameter("repok", repok);
        q.setParameter("dayname", dayname);
        return q.getResultList();
    }
    
     public static List<Commit> getAllCommits(int repod){
         TypedQuery<Commit> q = entityManager.createQuery("SELECT co FROM Commit co "
                + "JOIN Author we ON c.author = we.id "
                + "JOIN Branch fg ON c.branch = fg.id "
                + "JOIN Repo m ON b.Repo_ID = m.id "
                + "WHERE m.id = :repod ", Commit.class);

        q.setParameter("repod", repod);       
        return q.getResultList();
     }
     
     public static List<Commit> getCommitsByAuthor(int repoq, String name){
         TypedQuery<Commit> q = entityManager.createQuery("SELECT com FROM Commit com "
                + "JOIN Author ab ON c.author = ab.id "
                + "JOIN Branch dc ON c.branch = dc.id "
                + "JOIN Repo j ON b.Repo_ID = j.id "
                + "WHERE j.id = :repoq "
                 + "AND ab.name Like :name", Commit.class);

        q.setParameter("repoq", repoq);   
        q.setParameter("name","%" + name + "%");    
        return q.getResultList();
     }
     
     public static List<Commit> getCommitsByMessage(int repov, String message){
         TypedQuery<Commit> q = entityManager.createQuery("SELECT ca FROM Commit ca "
                + "JOIN Author ac ON c.author = ac.id "
                + "JOIN Branch cv ON c.branch = cv.id "
                + "JOIN Repo t ON b.Repo_ID = t.id "
                + "WHERE t.id = :repov "
                 + "AND ca.message LIKE :message", Commit.class);

        q.setParameter("repov", repov);   
        q.setParameter("message", "%"+message+"%");    
        return q.getResultList();
     }
     
     public static List<String> getAllBranches(int repou){
         TypedQuery<String> q = entityManager.createQuery("SELECT b.name FROM Branch b "
                + "JOIN Repo c ON b.Repo_ID = c.id "
                + "WHERE c.id = :repou ", String.class);

        q.setParameter("repou", repou);       
        return q.getResultList();
     }
     
     public static List<Commit> getLastCommits(int repog){
         TypedQuery<Commit> q = entityManager.createQuery("SELECT cv FROM Commit cv "
                + "JOIN Author ax ON c.author = ax.id "
                + "JOIN Branch zx ON c.branch = zx.id "
                + "JOIN Repo n ON b.Repo_ID = n.id "
                + "WHERE n.id = :repog "
                + "ORDER BY cv.comDate DESC ", Commit.class);

        q.setParameter("repog", repog);       
        return q.setMaxResults(5).getResultList();
     }
     
     public static String getCommitAuthor(String hash){
         TypedQuery<String> q = entityManager.createQuery("SELECT a.name FROM Author a "
                + "JOIN Commit c ON c.author = a.id "
                + "WHERE c.hash = :hash ", String.class);
     
        q.setParameter("hash", hash);      
        return q.getSingleResult();
     }
}
