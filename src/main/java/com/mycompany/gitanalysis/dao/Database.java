package com.mycompany.gitanalysis.dao;

import com.mycompany.gitanalysis.database.Author;
import com.mycompany.gitanalysis.database.Branch;
import com.mycompany.gitanalysis.database.Commit;
import com.mycompany.gitanalysis.database.Repo;
import java.sql.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import static com.mycompany.gitanalysis.app.App.getEM;

public class Database {

    static EntityManager entityManager = getEM();

    public static Repo insertRepo(String path) {
        Repo repo = repoExist(path);
        if (repo == null) {
            repo = new Repo();
            repo.setPath(path);
            entityManager.persist(repo);
        }
        return repo;
    }

    public static Branch insertBranch(String name, Repo repository) {
        Branch branch = branchExist(name, repository.getID());
        if (branch == null) {
            branch = new Branch();
            branch.setName(name);
            branch.setRepo_ID(repository);
            entityManager.persist(branch);
        } 
        return branch;
    }

    public static Author insertAuthor(String name, String email) {
        Author author = authorExist(email);
        if (author == null) {
            author = new Author();
            author.setName(name);
            author.setEmail(email);
            entityManager.persist(author);
        } 
        return author;
    }

    public static boolean insertCommits(String hash, Author author, Date d, String Message, Branch branch) {
        Commit commit = new Commit();
        if (!commitExist(hash)) {
            commit.setHash(hash);
            commit.setAuthor(author);
            commit.setDate(d);
            commit.setMessage(Message);
            commit.setBranch(branch);
            entityManager.persist(commit);
            return true;
        }
        return false;
    }

    public static Repo repoExist(String path) {
        TypedQuery<Repo> q = entityManager.createQuery("SELECT c FROM Repo c WHERE c.path = :path", Repo.class);
        q.setParameter("path", path);
        List<Repo> repo = q.getResultList();
        return repo.stream().findFirst().orElse(null);
    }

    public static boolean commitExist(String name) {
        TypedQuery<Commit> q = entityManager.createQuery("SELECT c FROM Commit c WHERE c.hash = :name", Commit.class);
        q.setParameter("name", name);
        List<Commit> res = q.getResultList();
        return res.size() == 1;
    }

    public static Author authorExist(String email) {
        TypedQuery<Author> q = entityManager.createQuery("SELECT c FROM Author c WHERE c.email = :email", Author.class);
        q.setParameter("email", email);
        List<Author> res = q.getResultList();
        return res.stream().findFirst().orElse(null);
    }

    public static Branch branchExist(String name, int repoid) {
        TypedQuery<Branch> q = entityManager.createQuery("SELECT c FROM Branch c "
                + "JOIN Repo r ON c.Repo_ID = r.id "
                + "WHERE c.name = :name "
                + "AND r.id = :repoid", Branch.class);
        q.setParameter("name", name);
        q.setParameter("repoid", repoid);
        List<Branch> res = q.getResultList();
        return res.stream().findFirst().orElse(null);
    }
}
