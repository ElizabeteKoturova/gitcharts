package com.mycompany.gitanalysis.database;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang3.ObjectUtils;

@Entity
@Table(name = "Commit")
public class Commit {

    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Hash")
    private String hash;

    @ManyToOne
    @JoinColumn(name = "Author_ID", referencedColumnName="ID")   
    private Author author;

    @Column(name = "Date")
    private Date comDate;

    @Column(name = "Message")
    private String message;

    @ManyToOne
    @JoinColumn(name = "Branch_ID", referencedColumnName="ID")
    private Branch branch;

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Author getAuthor() {
        return author;
    }

    public String getAuthorName() {
        return author == null ? "" : author.getName();
    }
    
    public void setAuthor(Author author) {
        this.author = author;
    }

    public Date getDate() {
        return ObjectUtils.clone(comDate);
    }

    public void setDate(Date comDate) {
        this.comDate = ObjectUtils.clone(comDate);

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }  
}
