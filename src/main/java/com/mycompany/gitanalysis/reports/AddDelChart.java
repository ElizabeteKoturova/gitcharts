package com.mycompany.gitanalysis.reports;

import static com.mycompany.gitanalysis.app.JfxApp.setRepo;
import static com.mycompany.gitanalysis.app.JfxApp.setRepo_id;
import static com.mycompany.gitanalysis.app.JfxApp.getEndDateChart;
import static com.mycompany.gitanalysis.app.JfxApp.getStartDateChart;
import com.mycompany.gitanalysis.database.Commit;
import static com.mycompany.gitanalysis.commits.FindDiff.countAdditions;
import static com.mycompany.gitanalysis.commits.FindDiff.countDeletions;
import static com.mycompany.gitanalysis.dao.Count.getCommitsByDate;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import org.eclipse.jgit.lib.Repository;

public class AddDelChart {
    
    public final static int REPO_ID = setRepo_id();
    public final static Repository REPO = setRepo();
    
    
    /**
     * Create and get chart view
     */
    public static VBox getAddDelChart() throws IOException {
        Group root = new Group();
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setTickLabelRotation(90);
        StackedBarChart<Date, Number> barChart = new StackedBarChart(xAxis, yAxis, getChartData());
        root.getChildren().add(barChart);
        barChart.setPrefHeight(1200);
        for (Node n : barChart.lookupAll(".default-color0.chart-bar")) {      //change bars color       
            n.setStyle("-fx-bar-fill: green;");            
        }
        for (Node n : barChart.lookupAll(".default-color1.chart-bar")) {            
            n.setStyle("-fx-bar-fill: red;");            
        }        
        for (Series<Date, Number> serie : barChart.getData()) {               //when mouse is entered on bar, 
            for (XYChart.Data<Date, Number> item : serie.getData()) {         //chart name changes to bar value
                item.getNode().setOnMouseEntered((MouseEvent event) -> {                    
                    barChart.setTitle(item.getYValue().toString());
                });
            }
        }
        barChart.setLegendVisible(false);
        barChart.setTitle("Additions and Deletions chart");
        barChart.setPrefHeight(1500);
        return new VBox(barChart);
    }
    
    
    /**
     * Gets and add repository data to chart 
     */
    private static ObservableList getChartData() throws IOException {
        ObservableList<XYChart.Series<Date, Number>> barChartData = FXCollections.observableArrayList();
        Date startDate = getStartDateChart();
        Date endDate = getEndDateChart();
        List<Commit> commits = getCommitsByDate(REPO_ID, startDate, endDate);
        
        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Additions");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < commits.size(); i++) {
            dataSeries1.getData().add(new XYChart.Data(df.format((commits.get(i).getDate())), countAdditions(commits.get(i).getHash(), REPO)));            
        }        
        barChartData.add(dataSeries1);        
        XYChart.Series dataSeries2 = new XYChart.Series();
        dataSeries2.setName("Deletions");
        for (int i = 0; i < commits.size(); i++) {
            dataSeries2.getData().add(new XYChart.Data(df.format((commits.get(i).getDate())), -countDeletions(commits.get(i).getHash(), REPO)));
        }
        barChartData.add(dataSeries2);
        return barChartData;
    }
}
