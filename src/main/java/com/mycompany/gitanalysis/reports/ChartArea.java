package com.mycompany.gitanalysis.reports;

import static com.mycompany.gitanalysis.app.JfxApp.setRepo_id;
import static com.mycompany.gitanalysis.app.JfxApp.getYear;
import static com.mycompany.gitanalysis.dao.Count.commitsInMonth;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;
import static com.mycompany.gitanalysis.dao.Count.getAuthorsEmailsByRepoIDByYear;
import java.text.DecimalFormat;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;

/**
 * Creates Area chart
 */
public class ChartArea {

    public final static int repo_id = setRepo_id();
    static int year = getYear();
    static List<String> authorList = getAuthorsEmailsByRepoIDByYear(repo_id, year);
    static String months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    /**
     * Creates Area chart view
     */
    public static VBox getAreaChart() {
        Group root = new Group();

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Month");
        yAxis.setLabel("Commits");

        AreaChart aC = new AreaChart(xAxis, yAxis, getChartData());
        root.getChildren().add(aC);
        aC.setPrefHeight(800);
        aC.setPrefHeight(1500);
        aC.setTitle("Commits per month");
        aC.setOnMousePressed((MouseEvent event) -> {

            Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());
            double x = xAxis.sceneToLocal(mouseSceneCoords).getX();
            double y = yAxis.sceneToLocal(mouseSceneCoords).getY();

            aC.setTitle(""
                    + xAxis.getValueForDisplay(x) + ",  "
                    + new DecimalFormat("##").format(yAxis.getValueForDisplay(y))
            );
        });
        return new VBox(aC);
    }

    /**
     * Gets repository data and adds it to chart
     */
    private static ObservableList getChartData() {
        ObservableList<XYChart.Series<String, Number>> areaChartData = FXCollections.observableArrayList();
        for (int i = 0; i < authorList.size(); i++) {
            XYChart.Series<String, Number> name = new Series<>();
            name.setName(authorList.get(i));
            for (int j = 0; j < months.length; j++) {
                name.getData().add(new XYChart.Data(months[j], commitsInMonth(year, repo_id, j + 1, authorList.get(i))));
            }
            areaChartData.add(name);
        }
        return areaChartData;
    }
}
