package com.mycompany.gitanalysis.reports;

import static com.mycompany.gitanalysis.app.JfxApp.setRepo_id;
import static com.mycompany.gitanalysis.dao.Count.commitsCount;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.VBox;
import static com.mycompany.gitanalysis.dao.Count.getAuthorsEmailsByRepoId;
import java.text.DecimalFormat;
import javafx.animation.TranslateTransition;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import static com.mycompany.gitanalysis.dao.Count.getCommitNumberByAuthorEmail;
import javafx.scene.control.CheckBox;


/**
* Creates pie chart
*/
public class ChartPie {

    public final static int REPO_ID = setRepo_id();   
    
    /**
     * Create chart view
     */
    public static VBox getPieChart() {
        PieChart chart;
        List<String> authorList = getAuthorsEmailsByRepoId(REPO_ID);
        long total = commitsCount(REPO_ID);
        double percent;

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        for (int i = 0; i < authorList.size(); i++) {                                           //count author percentage
            percent = (getCommitNumberByAuthorEmail(authorList.get(i), REPO_ID) * 100) / total;
            pieChartData.add(new PieChart.Data(authorList.get(i), percent));
        }

        chart = new PieChart(pieChartData);
        
        pieChartData.stream().forEach(pieData -> {                                             //Animation when mouse is clicked
            pieData.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                Bounds b1 = pieData.getNode().getBoundsInLocal();
                double newX = (b1.getWidth()) / 2 + b1.getMinX();
                double newY = (b1.getHeight()) / 2 + b1.getMinY();

                pieData.getNode().setTranslateX(0);
                pieData.getNode().setTranslateY(0);
                TranslateTransition tt = new TranslateTransition(
                        Duration.millis(1500), pieData.getNode());
                tt.setByX(newX);
                tt.setByY(newY);
                tt.setAutoReverse(true);
                tt.setCycleCount(2);
                tt.play();
            });
        });

        for (final PieChart.Data data : chart.getData()) {                                                //when mouse is entered,
            data.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {     //chart title changes
                @Override                                                                                 //to sector value
                public void handle(MouseEvent e) {                            
                    chart.setTitle(new DecimalFormat("##.##").format(data.getPieValue()) + "% " + data.getName()
                    );
                }
            });
        }

        chart.setTitle("Repository commits");
        VBox vbox = new VBox(chart);

        chart.setPrefHeight(1500);

        chart.setLabelsVisible(false);
        return vbox;
    }
}
