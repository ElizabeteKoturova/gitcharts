package com.mycompany.gitanalysis.reports;

import static com.mycompany.gitanalysis.app.JfxApp.setRepo;
import static com.mycompany.gitanalysis.app.JfxApp.setRepo_id;
import static com.mycompany.gitanalysis.dao.Count.getAllCommits;
import static com.mycompany.gitanalysis.dao.Count.getCommitsByAuthor;
import static com.mycompany.gitanalysis.dao.Count.getCommitsByMessage;
import com.mycompany.gitanalysis.database.Commit;
import static com.mycompany.gitanalysis.commits.FindDiff.diffCommit;
import com.mycompany.gitanalysis.database.Author;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import org.eclipse.jgit.lib.Repository;

public class CommitSearch {

    static final int repoId = setRepo_id();
    static List<Commit> commits = getAllCommits(repoId);
    static List<Commit> sortedCommits;
    private static TableView table = new TableView();
    static Repository repo = setRepo();
    static Label criteria = new Label();

    /**
     * Creates search view
     */
    static public VBox search() throws IOException {
        VBox vbox = new VBox();
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setEditable(false);
        table.setPlaceholder(new Label("No commits"));
        MenuItem menuItem1 = new MenuItem("Author");
        MenuItem menuItem2 = new MenuItem("Message");
        MenuItem menuItem3 = new MenuItem("Text");

        MenuButton menuButton = new MenuButton("Search by", null, menuItem1, menuItem2, menuItem3);

        final HBox hbox = new HBox();
        hbox.setSpacing(10);
        hbox.setPadding(new Insets(15, 12, 15, 12));

        menuItem1.setOnAction(event -> {
            Label label = new Label("Results:");
            label.setFont(new Font("Arial", 20));
            criteria.setText("Author name: ");
            TextField searchField = new TextField();
            Button search = new Button("Search");
            hbox.getChildren().clear();
            hbox.getChildren().addAll(criteria, searchField, search);
            search.setOnAction(action -> {

                createTable();
                table.setItems(searchByAuthor(searchField.getText()));
                table.setPrefHeight(1500);
                vbox.setSpacing(5);
                vbox.setPadding(new Insets(10, 0, 0, 10));
                vbox.getChildren().clear();
                vbox.getChildren().addAll(label, table);
            });
        });

        menuItem2.setOnAction(event -> {
            Label label = new Label("Results:");
            label.setFont(new Font("Arial", 20));
            criteria.setText("Message: ");
            TextField searchField = new TextField();
            Button search = new Button("Search");
            hbox.getChildren().clear();
            hbox.getChildren().addAll(criteria, searchField, search);
            search.setOnAction(action -> {
                createTable();

                table.setItems(searchMessage(searchField.getText()));
                table.setPrefHeight(1500);
                vbox.setSpacing(5);
                vbox.setPadding(new Insets(10, 0, 0, 10));
                vbox.getChildren().clear();
                vbox.getChildren().addAll(label, table);
            });
        });

        menuItem3.setOnAction(event -> {
            Label label = new Label("Results:");
            label.setFont(new Font("Arial", 20));
            criteria.setText("Search in commits: ");
            TextField searchField = new TextField();
            Button search = new Button("Search");
            hbox.getChildren().clear();
            hbox.getChildren().addAll(criteria, searchField, search);
            search.setOnAction(action -> {
                createTable();

                try {
                    table.setItems(searchText(searchField.getText()));
                } catch (IOException ex) {
                    Logger.getLogger(CommitSearch.class.getName()).log(Level.SEVERE, null, ex);
                }
                table.setPrefHeight(1500);
                vbox.setSpacing(5);
                vbox.setPadding(new Insets(10, 0, 0, 10));
                vbox.getChildren().clear();
                vbox.getChildren().addAll(label, table);
            });
        });
        return new VBox(menuButton, hbox, vbox);
    }

    /**
     * Gets all commits with authors name, which contains user entered text
     */
    static ObservableList<Commit> searchByAuthor(String name) {
        sortedCommits = getCommitsByAuthor(repoId, name);
        return FXCollections.observableArrayList(sortedCommits);
    }

    /**
     * Gets all commits with message, which contains user entered text
     */
    private static ObservableList searchMessage(String text) {
        sortedCommits = getCommitsByMessage(repoId, text);
        return FXCollections.observableArrayList(sortedCommits);
    }

    /**
     * Gets all commits, which contains user entered text
     */
    private static ObservableList searchText(String text) throws IOException {
        sortedCommits = getAllCommits(repoId);
        for (Commit commit : commits) {
            String diff = diffCommit(commit.getHash(), repo);
            if (!diff.contains(text)) {
                sortedCommits.remove(commit);
            }
        }
        return FXCollections.observableArrayList(sortedCommits);
    }

    /**
     * Reduce commit hash
     */
    public static String[] createHash(List<Commit> list) {
        String[] hashes = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            hashes[i] = list.get(i).getHash().substring(0, 7);
        }
        return hashes;
    }

    /**
     * Creates table to show search results
     */
    private static void createTable() {
        table.getColumns().clear();
        TableColumn hash = new TableColumn("Commit hash");
        hash.setCellValueFactory(new PropertyValueFactory<>("hash"));       
        TableColumn<Author, String> name = new TableColumn("Author name");
        name.setCellValueFactory(new PropertyValueFactory<>("authorName"));
        TableColumn message = new TableColumn("message");
        message.setCellValueFactory(new PropertyValueFactory<>("message"));
        TableColumn date = new TableColumn("Date");
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        table.getColumns().addAll(hash, name, message, date);
    }
}
