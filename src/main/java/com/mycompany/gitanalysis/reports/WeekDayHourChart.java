package com.mycompany.gitanalysis.reports;

import static com.mycompany.gitanalysis.app.JfxApp.setGit;
import static com.mycompany.gitanalysis.app.JfxApp.setRepo_id;
import static com.mycompany.gitanalysis.commits.GitCommits.CountCommitsPerHour;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

/**
 * Creates weekday and hour chart
 */
public class WeekDayHourChart {

    public static final Git git = setGit();
    public static final int repo_id = setRepo_id();
    static String days[] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    static String hours[] = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};

    /**
     * Creates chart view
     */
    public static VBox getWeekdayChart() throws IOException, GitAPIException {
        Group root = new Group();
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final StackedBarChart<String, Number> bc = new StackedBarChart(xAxis, yAxis, getChartData());
        bc.setTitle("Commits by Weekday and Hour");
        xAxis.setLabel("Hours");
        yAxis.setLabel("Commits");
        root.getChildren().add(bc);
        bc.setPrefHeight(1500);
        return new VBox(bc);
    }

    /**
     * Gets repository data to add it to chart
     */
    private static ObservableList getChartData() throws IOException, GitAPIException {
        ObservableList<XYChart.Series<String, Number>> barChartData = FXCollections.observableArrayList();

        for (int i = 0; i < days.length; i++) {
            XYChart.Series<String, Number> name = new Series<>();
            name.setName(days[i]);
            for (int j = 0; j <= 23; j++) {
                name.getData().add(new XYChart.Data(hours[j], CountCommitsPerHour(days[i], git, hours[j], repo_id)));
            }
            barChartData.add(name);
        }
        return barChartData;
    }
}
