package com.mycompany.gitanalysis.app;

import static com.mycompany.gitanalysis.app.JfxApp.generateRepoName;
import static com.mycompany.gitanalysis.app.JfxApp.getEM;
import static com.mycompany.gitanalysis.app.JfxApp.getEndDateChart;
import static com.mycompany.gitanalysis.app.JfxApp.getPath;
import static com.mycompany.gitanalysis.app.JfxApp.getStartDateChart;
import static com.mycompany.gitanalysis.app.JfxApp.getYear;
import static com.mycompany.gitanalysis.app.JfxApp.setGit;
import static com.mycompany.gitanalysis.app.JfxApp.setRepo;
import static com.mycompany.gitanalysis.app.JfxApp.setRepoDB;
import static com.mycompany.gitanalysis.app.JfxApp.setRevWalk;
import java.sql.Date;
import java.time.LocalDate;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author koram
 */
public class MyAppTest {

    @Test
    public static void testGenerateRepoName() {
    assertEquals("test", generateRepoName("String/to/test/.git"));
    }
    
    @Test
    public static void testGetStartDateChart(){
        JfxApp.value = LocalDate.now();
        Date date = new Date(Date.valueOf(LocalDate.now()).getTime());     
        assertEquals(date, getStartDateChart());
            }
    
    @Test
    public static void testGetEndDateChart(){
        JfxApp.value1 = LocalDate.now();
        Date date = new Date(Date.valueOf(LocalDate.now()).getTime());     
        assertEquals(date, getEndDateChart());
            }
    
    @Test
    public static void testGetYear(){
        JfxApp.year = "2018";
        assertEquals(2018, getYear());
    }
    
    @Test
    public static void testGetPath(){
        String path = "path/to/test/.git";
        assertEquals(path, getPath("path\\to\\test"));
    }
    
    @Test
    public static void testSetters(){
        JfxApp.repo = null;
        JfxApp.repoDB = null;
        JfxApp.entityManager = null;
        JfxApp.walk = null;
        JfxApp.git = null;
        
        assertEquals(null, setRepo());
        assertEquals(null, setRepoDB());
        assertEquals(null, getEM());
        assertEquals(null, setRevWalk());
        assertEquals(null, setGit());
    }
}
