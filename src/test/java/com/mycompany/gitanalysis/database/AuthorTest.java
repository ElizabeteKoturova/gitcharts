package com.mycompany.gitanalysis.database;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class AuthorTest {        
    
    String test = "Test";
    
    @Test
    public void testSetID() {               
    Author author = new Author();   
    author.setID(1);
    int res = author.getID();
    assertEquals(1, res);
    }
    
    @Test
    public void testSetName() {               
    Author author = new Author();   
    author.setName(test);
    String res = author.getName();
    assertEquals(test, res);
    }
    
    @Test
    public void testSetEmail() {               
    Author author = new Author();   
    author.setEmail(test);
    String res = author.getEmail();
    assertEquals(test, res);
    }
}
