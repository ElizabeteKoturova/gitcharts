package com.mycompany.gitanalysis.database;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class BranchTest {        
    
    String test = "Test";
    
    @Test
    public void testSetID() {               
    Branch branch = new Branch();   
    branch.setID(1);
    int res = branch.getID();
    assertEquals(1, res);
    }
    
    @Test
    public void testSetName() {               
    Branch branch = new Branch();   
    branch.setName(test);
    String res = branch.getName();
    assertEquals(test, res);
    }
    
    @Test
    public void testSetRepoId() { 
    Repo repo = new Repo();
    repo.setID(1);
    Branch branch = new Branch();   
    branch.setRepo_ID(repo);
    int res = branch.getRepo_ID().getID();
    assertEquals(1, res);
    }
}
