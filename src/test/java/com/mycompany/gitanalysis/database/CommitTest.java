package com.mycompany.gitanalysis.database;

import java.util.Date;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class CommitTest {        
    
    String test = "Test";
    
    @Test
    public void testID() {               
    Commit commit = new Commit();   
    commit.setID(1);
    int res = commit.getID();
    assertEquals(1, res);
    }
    
    @Test
    public void testHash() {               
    Commit commit = new Commit();   
    commit.setHash(test);
    String res = commit.getHash();
    assertEquals(test, res);
    }
    
    @Test
    public void testMessage() {               
    Commit commit = new Commit();   
    commit.setMessage(test);
    String res = commit.getMessage();
    assertEquals(test, res);
    }
    
    @Test
    public void testDate() {               
    Commit commit = new Commit(); 
    Date today = new Date(1000L);
    commit.setDate(new java.sql.Date(today.getTime()));
    Date res = commit.getDate();
    assertEquals(today, (java.util.Date)res);
    }
    
    @Test
    public void testBranch() {
        Branch bra = new Branch();
        bra.setID(2);
    Commit commit = new Commit();
    commit.setBranch(bra);
    int res = commit.getBranch().getID();
    assertEquals(2, res);
    }
    
    @Test
    public void testAuthor() { 
    Author auth = new Author();
    auth.setName(test);
    Commit commit = new Commit();    
    commit.setAuthor(auth);
    String res = commit.getAuthor().getName();
    assertEquals(test, res);
    assertEquals(test , commit.getAuthorName());
    }
}