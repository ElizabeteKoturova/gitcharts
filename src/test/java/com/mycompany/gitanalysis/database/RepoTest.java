package com.mycompany.gitanalysis.database;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class RepoTest {        
    
    String test = "Test";
    
    @Test
    public void testID() {               
    Repo repo = new Repo();   
    repo.setID(1);
    int res = repo.getID();
    assertEquals(1, res);
    }
    
    @Test
    public void testPath() {               
    Repo repo = new Repo();   
    repo.setPath(test);
    String res = repo.getPath();
    assertEquals(test, res);
    }
}